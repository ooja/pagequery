package pagequery

import (
	"github.com/PuerkitoBio/goquery"
	"reflect"
	"errors"
	"regexp"
	"strings"
	util "bitbucket.org/ooja/common"
	"fmt"
	"strconv"
	"time"
)

const (
	CurrenciesUnicode = "\u0024\u20a6"
)

func Log(args ...interface{}){
	fmt.Println(args...)
}

type Analyser struct {
	Result interface{}
}

// remove excess white spaces
func RemoveExcessWhiteSpace(text string) string {
	r, _ := regexp.Compile("\\s+")
	return strings.TrimSpace(r.ReplaceAllString(text, " "))
}

// remove punctuations from text
func RemovePunctuations(text string) string {
	r, _ := regexp.Compile("[^\\w\\s]")
	return r.ReplaceAllString(text, "")
}

// remove substring from a string using a regex
func RemoveByRegex(text, rx string) string {
	r, _ := regexp.Compile(rx)
	return r.ReplaceAllString(text, "")
}

// given a string, extract price
func ExtractPrice(str string) []string {
	r, _ := regexp.Compile("(?P<currency>["+CurrenciesUnicode+"N]{1})\\s*(?P<amount>(?:[0-9]+[,.]{0,1})+)")
	return r.FindStringSubmatch(str)
}

// call an analyser function
func (analyser *Analyser) Call(name string, parameters map[string]interface{}, args...interface{}) error {

	// construct argument for reflection call
	inputs := make([]reflect.Value, len(args) + 1)
	inputs[0] = reflect.ValueOf(parameters)
    for i, _ := range args {
      	inputs[i+1] = reflect.ValueOf(args[i])
  	}
	val := reflect.ValueOf(analyser)	

	// ensure the analyser has a method matching the name passed
	if val.MethodByName(name) != reflect.ValueOf(nil) {
		val.MethodByName(name).Call(inputs)
		return nil

	} else {
		return errors.New("analyser method with name ["+name+"] doesn't exist")				
	}
}

// get src values from a collection of img elements
// sets analyser.Result to a slice of image urls
// if no image is found, analyser.Result is set to an empty string slice
func (analyser *Analyser) GetSRC(parameters map[string]interface{}, elements *goquery.Selection) {
	results := []string{}
	elements.Each(func (i int, s *goquery.Selection){
		val, exists := s.Attr("src")
		if exists {
			results = append(results, val)
		}
	})
	analyser.Result = results
}

// get src value from the first img element found
// sets analyser.Result to the src value
// if no image is found, analyser.Result is set to an empty string 
func (analyser *Analyser) GetOneSRC(parameters map[string]interface{}, elements *goquery.Selection) {
	analyser.Result = ""
	elements.Eq(0).Each(func (i int, s *goquery.Selection){
		val, exists := s.Attr("src")
		if exists {
			analyser.Result = val
		}
	})
}

// if analyser.Result is a slice of string and contains atleast one value
// it will set analyser.Result to the first value of the slice of string
// it will set analyser.Result to an empty string if analyzer.Result is not
// a slice of string
func (analyser *Analyser) SetToFirstResult(parameters map[string]interface{}, elements *goquery.Selection) {
	switch v := analyser.Result.(type) {
		case []string:
			if len(v) > 0 {
				analyser.Result = v[0]
				return
			}
	}
	analyser.Result = ""
}

// looks in the first index of analyzer.Result and extracts and parse
// price information. Sets analyser.Result to be a map containing amount and currecy.
// assumes analyzer.Result to be a slice of string, otherwise analyzer.Result is set to an empty map of string key and interface{} value
func (analyser *Analyser) GetStandardPrice(parameters map[string]interface{}, elements *goquery.Selection) {
	newResult := make(map[string]interface{})
	switch results := analyser.Result.(type) {
	case []string:
		if len(results) > 0 {
			str := results[0]
			priceInfo := ExtractPrice(str)
			if len(priceInfo) == 3 {
				currency := priceInfo[1]
				amount := priceInfo[2]
				amount = strings.Replace(amount, ",", "", -1)
				newPriceUntruncated, err := strconv.ParseFloat(amount, 64)
				newPriceTruncated := float64(int(newPriceUntruncated * 100)) / 100
				if err == nil {
					newResult["currency"] = CurrencySymToAbbr[currency]
					newResult["amount"] = newPriceTruncated
				}
			}
		}
	}
	analyser.Result = newResult
}

// Find all img elements and get all src values
// sets analyser.Result to a slice of image urls 
// if no image is found, analyser.Result is set to an empty string slice
func (analyser *Analyser) GetSRCOfAllImages(parameters map[string]interface{}, elements *goquery.Selection) {
	results := []string{}
	elements.Find("img").Each(func (i int, s *goquery.Selection){
		attr, exists := s.Attr("src")
		if exists {
			results = append(results, attr)
		}
	})
	analyser.Result = results
}

// get text from a collection of elements
// sets analyser.Result to a slice of text containing text extracted
// from each element
// sets analyser.Result to a empty string slice if no result is found 
func (analyser *Analyser) GetText(parameters map[string]interface{}, elements *goquery.Selection) {
	results := []string{}
	elements.Each(func (i int, s *goquery.Selection){
		results = append(results, RemoveExcessWhiteSpace(s.Text()))
	})
	analyser.Result = results
}

// sets analyser.Result to the first index.f
// expects analyser.Result to be a slice.
// if slice is empty, analyser.Result is set to 
// the empty/nil variant of the matching slice type
func (analyser *Analyser) UseFirstIndexOfSlice(parameters map[string]interface{}, elements *goquery.Selection) {
	switch results := analyser.Result.(type) {
	case [][]string:
		if len(results) > 0 {
			analyser.Result = results[0]
			return
		}
		analyser.Result = []string{}
	case []string:
		if len(results) > 0 {
			analyser.Result = results[0]
			return
		}
		analyser.Result = ""
	case []map[int][]string:
		if len(results) > 0 {
			analyser.Result = results[0]
			return
		}
		analyser.Result = map[int][]string{}
	}
}

// get href attribute of a collection of elements
// sets analyser.Result to a slice of string containing urls
// sets analyser.Result to an empty slice of string if nothing was found 
func (analyser *Analyser) GetHREF(parameters map[string]interface{}, elements *goquery.Selection) {
	results := []string{}
	elements.Each(func (i int, s *goquery.Selection){
		href, exists := s.Attr("href")
		if exists {
			results = append(results, href)
		}
	})
	analyser.Result = results
}


// iterates over analyser.Result and trims only string items
// assumes analyser.Result contains a slice of strings.
func (analyser *Analyser) Trim(parameters map[string]interface{}, elements *goquery.Selection) {
	for i, item := range analyser.Result.([]string) {
		analyser.Result.([]string)[i] = RemoveExcessWhiteSpace(item)
	}
}

// alias: Trim
func (analyser *Analyser) TrimString(parameters map[string]interface{}, elements *goquery.Selection) {
	analyser.Trim(parameters, elements)
}

// iterates over analyser.Result, removing any matching terms passed 
// in the parameter's "terms" list/slice.
// expects analyser.Result to be a slice of strings
// replaces each analyser.Result item with the newly trimmed equivalent 
func (analyser *Analyser) TrimTerms(parameters map[string]interface{}, elements *goquery.Selection) {
	if parameters["terms"] != nil {
		switch terms := parameters["terms"].(type) {
		case []interface{}:
			for i, v := range analyser.Result.([]string) {
				newValue := v
				for _, t := range terms {
					r, _ := regexp.Compile("(?i)" + t.(string))
					newValue = r.ReplaceAllString(newValue, "")
				}
				analyser.Result.([]string)[i] = newValue
			}
		}
	}
}

// iterates over analyser.Result, extract a specific attribute from
// each elements. Attribute to extract is specified in the "attr" field of paramaters
// analyser.Result is set to a slice of string containing the attr for each element
func (analyser *Analyser) GetAttr(parameters map[string]interface{}, elements *goquery.Selection) {
	results := []string{}
	if parameters["attr"] != nil && parameters["attr"].(string) != "" {
		elements.Each(func (i int, s *goquery.Selection){
			attr, _ := s.Attr(parameters["attr"].(string))
			results = append(results, attr)
		})
	}
	analyser.Result = results
}

// extracts features from all `li` tags of the selections
// expects text in `li` tags to be in this form "label:value"
// sets analyser.Result to a map of feature label and value and an `others`
// field containing features/text that are'nt in the expected format
// e.g map[color: red, material: leather, others: ["high quality","apparel"]]
func (analyser *Analyser) UL_GetKeyFeatures(parameters map[string]interface{}, elements *goquery.Selection) {
	features := make(map[string]interface{})
	var others []string
	elements.Each(func (i int, s *goquery.Selection){
		s.Find("li").Each(func (
			i int, s *goquery.Selection){
			featureTxt := s.Text()
			featureData := strings.Split(featureTxt, ":")
			if len(featureData) > 1 {
				features[strings.TrimSpace(strings.ToLower(featureData[0]))] = strings.TrimSpace(featureData[1])
			} else {
			 	others = append(others, strings.TrimSpace(strings.ToLower(featureData[0])))
			}
		})
	})
	features["others"] = others
	analyser.Result = features
}


// iterates over each elements, extracting full product details from konga
// sets analyser.Result to a map of features
// e.g map[color: red, material: leather, others: ["high quality","apparel"]]
// "others" key in result holds standalone features that do not have a key
func (analyser *Analyser) Table_GetFullFeatures(parameters map[string]interface{}, elements *goquery.Selection) {
	features := make(map[string]interface{})
	var others []string
	elements.Each(func (i int, s *goquery.Selection){
		s.Find("tr").Each(func (i int, s *goquery.Selection){
			key := s.Find("td:first-child,th:first-child").Text()
			value := s.Find("td:nth-child(2)").Text()
			if key != "" {
				if value != "" {
					features[RemoveExcessWhiteSpace(strings.ToLower(key))] = RemoveExcessWhiteSpace(strings.ToLower(value))
				} else {
					others = append(others, RemoveExcessWhiteSpace(strings.ToLower(key)))
				}
			}
		})
	})
	features["others"] = others
	analyser.Result = features
}

// sets default location passed in parameters
// analyser.Result is filled with the data passed in parameters
func (analyser *Analyser) SetDefaultLocation(parameters map[string]interface{}, elements *goquery.Selection) {
	
	analyser.Result = make(map[string]string)
	
	if parameters["country"] != nil {
		analyser.Result.(map[string]string)["country"] = parameters["country"].(string)
	}

	if parameters["state"] != nil {
		analyser.Result.(map[string]string)["state"] = parameters["state"].(string)
	}

	if parameters["city"] != nil {
		analyser.Result.(map[string]string)["city"] = parameters["city"].(string)
	}

	if parameters["town"] != nil {
		analyser.Result.(map[string]string)["town"] = parameters["town"].(string)
	}
}

// get the items(li) of an unordered list(ul).
// excludes an item if it has an attribute name matching the value of parameters['disableAttr'], acommpanied
// with a matching value of parameters['disableAttrVal'].
// Expects elements to contain a selection of <ul> elements.
// analyser.Result is set to a slice of slice of string 
func (analyser *Analyser) UL_GetItemsText(parameters map[string]interface{}, elements *goquery.Selection) {
	var results [][]string
	disableAttr := util.GetOrDefault(parameters, "disableAttr", "").(string)
	disableAttrVal := util.GetOrDefault(parameters, "disableAttrVal", "").(string)
	elements.Each(func (i int, s *goquery.Selection){
		result := []string{}
		s.Find("li").Each(func (i int, s *goquery.Selection){
			if disableAttrVal != "" {
				if attrVal, _ := s.Attr(disableAttr); !strings.Contains(attrVal, disableAttrVal) { //TODO: use regex
					if cleanVal := RemoveExcessWhiteSpace(RemovePunctuations(s.Text())); cleanVal != "" {
						result = append(result, s.Text())
					}
				}
			} else {
				if cleanVal := RemoveExcessWhiteSpace(RemovePunctuations(s.Text())); cleanVal != "" {
					result = append(result, s.Text())
				}
			}
		})
		results = append(results, result)
	})
	analyser.Result = results
}

// finds all children of selected parent elements, extract the text in elements
// that are enabled. 
// `childElemName` indicates the element to look for in the selected element
// `disableAttr` parameter specifies the attr that indicates element enable state
// `disableAttrVal` parameter specifies the value that signifies a disabled state
// the attribute value of `disableAttr` must match `disableAttrVal` to be indicate a disabled element
func (analyser *Analyser) GetEnabledChildrenText(parameters map[string]interface{}, elements *goquery.Selection) {
	var results [][]string
	childElemName := util.GetOrDefault(parameters, "childElemName", "").(string)
	disableAttr := util.GetOrDefault(parameters, "disableAttr", "").(string)
	disableAttrVal := util.GetOrDefault(parameters, "disableAttrVal", "").(string)
	elements.Each(func (i int, s *goquery.Selection){
		result := []string{}
		s.Find(childElemName).Each(func (i int, s *goquery.Selection){
			if disableAttrVal != "" {
				if attrVal, _ := s.Attr(disableAttr); !strings.Contains(attrVal, disableAttrVal) { //TODO: use regex
					if cleanVal := RemoveExcessWhiteSpace(RemovePunctuations(s.Text())); cleanVal != "" {
						result = append(result, RemoveExcessWhiteSpace(s.Text()))
					}
				}
			} else {
				if cleanVal := RemoveExcessWhiteSpace(RemovePunctuations(s.Text())); cleanVal != "" {
					result = append(result, RemoveExcessWhiteSpace(s.Text()))
				}
			}
		})
		results = append(results, result)
	})
	analyser.Result = results
}

// creates the expected available size result type
// expects Analyser.Result to be a slice of string (the available size) which
// was computed by other funcs.
// Accepts the following parameter
// 	`sizeUnit` is the unit of sizes e.g uk,us,ml,oz
// sets Analyser.Result to map[sizes: [1,2] unit:uk]
func (analyser *Analyser) PackageAvailableSize(parameters map[string]interface{}, elements *goquery.Selection) {
	sizeUnit := util.GetOrDefault(parameters, "sizeUnit", "").(string)
	analyser.Result = map[string]interface{}{
		"sizes": analyser.Result.([]string),
		"unit": sizeUnit,
	}
}

// get all table <td> in all selected elements
// sets Analyser.Result to a map where the key is the index to the table row
// and the value is a string slice of the table data <td> found in the table rows <tr>
// e.g map[1:[data1 data2 data3], 2:[data1 data2 data3]]
func (analyser *Analyser) GetAllTD(parameters map[string]interface{}, elements *goquery.Selection) {
	var newResult []map[int][]string
	elements.Each(func (i int, s *goquery.Selection){
		currentTable := make(map[int][]string)
		s.Find("tr").Each(func (i int, s *goquery.Selection){
			var rowData []string
			dataSelector := "td"

			// also find <th> if enabled
			if parameters["include_th"] == true {
				dataSelector = "td,th"
			} 

			s.Find(dataSelector).Each(func (i int, s *goquery.Selection){
				rowData = append(rowData, s.Text())
			})
			currentTable[i] = rowData
		})
		newResult = append(newResult, currentTable)
	})
	analyser.Result = newResult
}

// Gets a feature by it's key/name which is specified in parameters
// expects Analyser.Result to be feature extracted using any analyser function for feature retrieval
// expects Analyser.Result to be of type map[string]interface{}
func (analyser *Analyser) GetFromFeatures(parameters map[string]interface{}, elements *goquery.Selection) {
	newResult := ""
	switch features := analyser.Result.(type) {
	case map[string]interface{}:
		key := util.GetOrDefault(parameters, "name", "").(string)
		if features[key] != nil {
			switch val := features[key].(type) {
			case string:
				newResult = val
			}
		}
	}
	analyser.Result = newResult
}

// takes the string value of Analyser.Result and adds it into a slice and
// sets slice as new value of Analyser.Result
// if value type of Analyser.Result is not know, then it is set to an empty array of the matching type
func (analyser *Analyser) ToStringSlice(parameters map[string]interface{}, elements *goquery.Selection) {
	switch val := analyser.Result.(type) {
	case string:
		if val != "" {
			analyser.Result = []string{val}
			return;
		}
	}
	analyser.Result = []string{}
}

// extracts substrings from Analyser.Result using a regex passed in parameters.
// if Analyser.Result is a string type, set to an slice of the captured groups of the containing string.
// if Analyser.Result is a slice of string, set to a slice of captured groups of reach string in the slice
// if no captured group is found, Analyser.Result is set to an empty array of its respective type
// Analyser.Result is set to an error if type is not supported
func (analyser *Analyser) GetByRegex(parameters map[string]interface{}, elements *goquery.Selection) {
	
	// get regex from parameters. required
	rx := util.GetOrDefault(parameters, "rx", "").(string)
	if rx == "" {
		analyser.Result = errors.New("GetByRegex: rx parameter is required")
		return
	}

	r, err := regexp.Compile(rx)
	if err != nil {
		analyser.Result = errors.New("GetByRegex: " + err.Error())
		return
	}

	switch data := analyser.Result.(type) {

	case string:
		if data != "" {
			analyser.Result = r.FindStringSubmatch(data)
			return
		}

	case []string:

		newResult := make([][]string, 0)
		for _, v := range data {
			if  v != "" {
				match := r.FindStringSubmatch(v)
				if len(match) > 0 {
					newResult = append(newResult, match)
				}
			}
		}
		analyser.Result = newResult

	default:
		analyser.Result = errors.New("GetByRegex: cannot work with type of Analyser.Result")
	}
}

// get an index from Analyser.Result and sets Analyser.Result to the value.
// if index does not exist/out of range, Analyser.Result is set to the provided default in parameters
// if index is not passed in parameter, set Analyser.Result to an error
func (analyser *Analyser) GetIndexOrDefault(parameters map[string]interface{}, elements *goquery.Selection) {
		
	defaultVal := util.GetOrDefault(parameters, "default", "").(string)
	index := util.GetOrDefault(parameters, "index", -1).(float64)
	if index == -1 {
		analyser.Result = errors.New("GetIndexOrDefault: index not provided in parameters")
		return
	}
	
	switch data := analyser.Result.(type) {
	case []string:

		// convert index to int
		i := int(index)

		// ensure index is not out of range
		if i <= (len(data) - 1) {
			analyser.Result = data[i]
			return	

		} else {
			analyser.Result = defaultVal
		}

	default:
		analyser.Result = errors.New("GetIndexOrDefault: cannot work with type of Analyser.Result")
	}
}

// tries to parse date to RFC3339 date format
// expects Analyser.Result to be a string representing a date format 
// that is understood by DateParser.
// sets Analyser.Result to an RFC3339 date. e.g 2006-01-02T15:04:05Z07:00 .
// use timezone passed in parameter else use "Africa/Lagos" as default
// if unable to parse data, an error is assigned to Analyser.Result
func (analyser *Analyser) GetDate(parameters map[string]interface{}, elements *goquery.Selection) {
	timezone := util.GetOrDefault(parameters, "timezone", "Africa/Lagos").(string)
	switch val := analyser.Result.(type) {
	case string:
		if val != "" {
			dp := new(DateParser)
			utcTime, err := dp.Parse(val, timezone)
			if err == nil {
				analyser.Result = utcTime.Format(time.RFC3339)
			} else {
				analyser.Result = errors.New("GetDate: " + err.Error())
			}
		}
	default:
		analyser.Result = errors.New("GetDate: cannot work with type of Analyser.Result")
	}
}
