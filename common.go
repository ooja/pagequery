package pagequery

import (
	"strings"
)

var NigeriaStateList []string
var CurrencySymToAbbr map[string]string

func init() {
	nigeriaStateList := "ABIA,ADAMAWA,AKWA IBOM,ANAMBRA,BAUCHI,BAYELSA,BENUE,BORNO,CROSS RIVER,DELTA,EBONYI,EDO,EKITI,ENUGU,GOMBE,IMO,JIGAWA,KADUNA,KANO,KATSINA,KEBBI,KOGI,KWARA,LAGOS,NASSARAWA,NIGER,OGUN,ONDO,OSUN,OYO,PLATEAU,RIVERS,SOKOTO,TARABA,YOBE,ZAMFARA"
	for _, state := range strings.Split(nigeriaStateList, ",") {
		state = strings.ToLower(state)
		NigeriaStateList = append(NigeriaStateList, state)
	}
}

func init(){
	CurrencySymToAbbr = make(map[string]string)
	CurrencySymToAbbr["$"] = "USD"
	CurrencySymToAbbr["₦"] = "NGN"
	CurrencySymToAbbr["N"] = "NGN"
}