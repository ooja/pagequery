package pagequery

import(
	// "fmt"
	"regexp"
	"errors"
	"bitbucket.org/ooja/common"
	"strings"
	"time"
	"strconv"
)

var shortMonthToFull map[string]time.Month

func init(){
	shortMonthToFull = make(map[string]time.Month)
	shortMonthToFull["jan"] = time.January
	shortMonthToFull["feb"] = time.February
	shortMonthToFull["mar"] = time.March
	shortMonthToFull["apr"] = time.April
	shortMonthToFull["may"] = time.May
	shortMonthToFull["jun"] = time.June
	shortMonthToFull["jul"] = time.July
	shortMonthToFull["aug"] = time.August
	shortMonthToFull["sep"] = time.September
	shortMonthToFull["oct"] = time.October
	shortMonthToFull["nov"] = time.November
	shortMonthToFull["dec"] = time.December
}

type DateParser struct{
	timezone string
	rxDateMap map[string]interface{}
}

// Initialize the date parser
func (dp *DateParser) init() {

	// to hold all date matching regex and their parsing functions
	dp.rxDateMap = make(map[string]interface{})

	// OLX date format
	dp.rxDateMap["(?i)^(Today|Yesterday),([0-9]{1,2}):([0-9]{1,2})$"] = dp.OLXDate
	dp.rxDateMap["(?i)^([0-9]{1,2})\\s*([a-z]{3})$"] = dp.OLXDate
}

// Parse date to utc
// Pass timezone of date
func (dp *DateParser) Parse(dateStr, timezone string) (time.Time, error) {

	t := time.Time{}
	err := errors.New("")

	// set timezone
	dp.timezone = timezone

	// initialize parser
	dp.init()

	if len(dp.rxDateMap) == 0 {
		return time.Time{}, errors.New("no rx in rx data map")
	}

	// find a matching regex
	for rx, f := range dp.rxDateMap {

		// compile regex
		r, e := regexp.Compile(rx)
		err = e
		if err != nil {
			return time.Time{}, err
		}

		// match against passed string
		match := r.FindStringSubmatch(dateStr)

		// call func only if there is a submatch
		if len(match) > 0 {
			return f.(func(match []string)(time.Time, error))(match)

		} else {
			err = errors.New("can't parse date using avaialable rx")
		}
	}

	return t, err
}

// Parses dates: "Today,02:20" and "8 Jun", 
// returns UTC time
func (dp *DateParser) OLXDate(match []string) (time.Time, error) {

	// expects 4 sub matches for this format = Today,02:20
	if len(match) == 4 {

		var newTime time.Time 
		d := match[1]
		day := 0

		hour, err := strconv.Atoi(match[2])
		if err != nil {
			return time.Time{}, err
		}

		minute, err := strconv.Atoi(match[3])
		if err != nil {
			return time.Time{}, err
		}

		// get current time in timezone
		t, err := common.GetCurrentTimeInZone(dp.timezone)
		if err != nil {
			return time.Time{}, err
		}

		// get day
		if strings.ToLower(d) == "today" {
			day = t.Day()
			newTime = time.Date(t.Year(), t.Month(), day, hour, minute, 0, 0, t.Location())

		} else if strings.ToLower(d) == "yesterday" {
			t = t.AddDate(0, 0, -1)
			newTime = time.Date(t.Year(), t.Month(), t.Day(), hour, minute, 0, 0, t.Location())
		}

		return newTime.UTC(), nil

	} else if len(match) == 3 {			

		// get current time in timezone
		t, err := common.GetCurrentTimeInZone(dp.timezone)
		if err != nil {
			return time.Time{}, err
		}

		day, err := strconv.Atoi(match[1])
		if err != nil {
			return time.Time{}, err
		}

		month := match[2]
		newTime := time.Date(t.Year(), shortMonthToFull[strings.ToLower(month)], day, 0, 0, 0, 0, t.Location())
		return newTime.UTC(), nil

	} else {
		return time.Time{}, errors.New("unexpected number of matches")
	}

	return time.Time{}, nil
}