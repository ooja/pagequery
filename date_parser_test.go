package pagequery

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"bitbucket.org/ooja/common"
	"time"
)

func TestOLXToday(t *testing.T) {

	assert := assert.New(t)
	testTime, err := common.GetCurrentTimeInZone("Africa/Lagos")
	assert.Nil(err)
	testTime = time.Date(testTime.Year(), testTime.Month(), testTime.Day(), 6, 30, 0, 0, testTime.Location()).UTC()
	
	dp := new(DateParser)
	utcTime, err := dp.Parse("Today,06:30", "Africa/Lagos")
	assert.Nil(err)
	assert.Equal(testTime.String(), utcTime.String())
}

func TestOLXYesterday(t *testing.T) {

	assert := assert.New(t)
	testTime, err := common.GetCurrentTimeInZone("Africa/Lagos")
	assert.Nil(err)
	testTime = testTime.AddDate(0, 0, -1)
	testTime = time.Date(testTime.Year(), testTime.Month(), testTime.Day(), 6, 30, 0, 0, testTime.Location()).UTC()
	
	dp := new(DateParser)
	utcTime, err := dp.Parse("Yesterday,06:30", "Africa/Lagos")
	assert.Nil(err)
	assert.Equal(testTime.String(), utcTime.String())
}

func TestOLXMonth(t *testing.T) {

	assert := assert.New(t)
	testTime, err := common.GetCurrentTimeInZone("Africa/Lagos")
	assert.Nil(err)
	testTime = time.Date(testTime.Year(), time.June, 6, 0, 0, 0, 0, testTime.Location()).UTC()
	
	dp := new(DateParser)
	utcTime, err := dp.Parse("6 Jun", "Africa/Lagos")
	assert.Nil(err)
	assert.Equal(testTime.String(), utcTime.String())
}