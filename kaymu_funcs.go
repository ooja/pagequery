package pagequery

import (
	"strings"
	"github.com/PuerkitoBio/goquery"
)

// creates a correct mapping of kaymu attributes/features
// expects Analyser.Result to be a parsed map of a table created using `GetAllTD`
// sets Analyser.Result to a map of features and values of type interface{}
func (analyser *Analyser) KaymuGetFullFeatures(parameters map[string]interface{}, elements *goquery.Selection) {
	newResult := make(map[string]interface{})
	switch tableData := analyser.Result.(type) {
	case map[int][]string:
		for _, tdValues := range tableData {
			// must be multiple of two
			if len(tdValues) % 2  == 0 {
				start := 0		
				for i := 0; i < (len(tdValues) / 2); i++ {
					featureLabel := strings.ToLower(RemoveByRegex(RemoveExcessWhiteSpace(tdValues[start]), "[^\\w\\s())]+$"))
					featureVal := RemoveExcessWhiteSpace(tdValues[start + 1])
					newResult[featureLabel] = featureVal
					start += 2
				}
			}
		}
	}
	analyser.Result = newResult
}

// creates location information 
// expects location data to be assigned to 
// Analyser.Result after being extracted with GetByRegex func
// Analyser.Result must be an array of string of captured group from GetByRegex func
// Analyser.Result must not be an error, else do nothing
func (analyser *Analyser) KaymuLocation(parameters map[string]interface{}, elements *goquery.Selection) {
	result := make(map[string]string)
	switch val := analyser.Result.(type) {
	case []string:
		// expects 3 captured groups
		if len(val) == 3 {
			result["state"] = RemoveExcessWhiteSpace(val[1])
			result["city"] = RemoveExcessWhiteSpace(val[2])
			result["country"] = "nigeria"		
		}
	}
	analyser.Result = result
}
