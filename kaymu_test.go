package pagequery

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestKaymuGetFullFeature(t *testing.T) {

	testHTML := `
		<table class="attributeTable">
			<tbody>
				<tr>
					<td class="attributeTitle">Bluetooth:</td>
					<td class="attributeValue">Yes</td>
					<td class="attributeTitle">Brand:</td>
					<td class="attributeValue">BlackBerry </td>
				</tr>
				<tr>
					<td class="attributeTitle">Color:</td>
					<td class="attributeValue">Black </td>
					<td class="attributeTitle">Condition:</td>
					<td class="attributeValue">New</td>
				</tr>
			<tbody>
		</table>
	`
	testQuery := `
	{
		"getFullFeatures": {
			"selector": "table",
			"funcs": ["GetAllTD","UseFirstIndexOfSlice","KaymuGetFullFeatures"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(len(result["getFullFeatures"].(map[string]interface{})), 0, "should not be empty")
	assert.Equal(result["getFullFeatures"].(map[string]interface{})["bluetooth"], "Yes", "should match")
	assert.Equal(result["getFullFeatures"].(map[string]interface{})["brand"], "BlackBerry", "should match")
}

func TestShouldGetKaymuLocation(t *testing.T) {

	testHTML := `
		<div class="product-location">Lagos - Iyanipaja</div>
	`
	testQuery := `
	{
		"getLocation": {
			"selector": "div.product-location",
            "funcs": ["GetText", "UseFirstIndexOfSlice", "GetByRegex:{ \"rx\":\"(?i)^([a-z0-9- ]+)[\\\\s]*-[\\\\s]*([a-z0-9- ]+)$\" }", "KaymuLocation"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.Equal(len(result["getLocation"].(map[string]string)), 3, "should have 3 items")
	assert.Equal(result["getLocation"].(map[string]string)["state"], "Lagos", "should match")
	assert.Equal(result["getLocation"].(map[string]string)["city"], "Iyanipaja", "should match")
	assert.Equal(result["getLocation"].(map[string]string)["country"], "nigeria", "should match")
}