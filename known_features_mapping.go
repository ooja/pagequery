// Contains descriptors on how to identify a known feature and
// how to extract the relevant value of the feature
package pagequery

// contains the terms descriptors
var KnownTermsData []map[string]string

func init(){

	// set the descriptors
	// `rxTerm` is used to match a potential feature text
	// `rxValue` is used to extract value and other data from the value associated to rxTerm
	// `term` is the actual name for the feature (this is indexed together with the values from rxValue)
	// `all` ensures all captured named group in `rxValue` is used as the value associated with `term`
	// if `rxValue` is omitted, `term` is associated with the actual value associated with `rxTerm`
	KnownTermsData = []map[string]string{
		map[string]string{
			"rxTerm": "(?i)r\\.?a\\.?m",
			"rxValue": "(?i)(?P<value>[0-9]+gb|mb)",
			"term": "ram",
		},
		map[string]string{
			"rxTerm": "(?i)colou?rs?",
			"term": "color",
		},
	}

}