package pagequery

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestFindInFeatures(t *testing.T) {

	knownTermsData := []map[string]string{
		map[string]string{
			"rxTerm": "(?i)r\\.?a\\.?m",
			"rxValue": "(?i)(?P<value>[0-9]+gb|mb)",
			"term": "ram",
		},
		map[string]string{
			"rxTerm": "(?i)colou?rs?",
			"term": "color",
		},
		map[string]string{
			"rxTerm": "(?i)r\\.?a\\.?m",
			"rxValue": "(?i)(?P<value>[0-9]+)(?P<unit>gb|mb)",
			"term": "ram2",
			"all": "1",
		},
		map[string]string{
			"rxTerm": "(?i)brand",
			"value": "blackberry",
			"term": "brand",
		},
	}

	sampleFeatures := map[string]interface{}{
		"ram": "3GB",
		"color": "red",
		"brand": "nokia",
		"others": []string{"free shipping"},
	}

	pageQuery := PageQuery{}
	result := pageQuery.FindInFeatures(sampleFeatures, knownTermsData)
	assert := assert.New(t)
	assert.Equal(result["ram"], "3GB", "must match")
	assert.Equal(result["color"], "red")
	assert.Equal(result["ram2"].(map[string]string)["value"], "3", "must match")
	assert.Equal(result["ram2"].(map[string]string)["unit"], "GB", "must match")
	assert.NotEqual(result["brand"], "nokia", "must not match")
}


