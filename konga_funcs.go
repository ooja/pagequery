package pagequery

import (
	"strings"
	"github.com/PuerkitoBio/goquery"
)

// takes the first selected element, gets the location information
// sets analyser.Result to slice of map containing town, city, state 
// e.g map[state:lagos country:nigeria others:""]
func (analyser *Analyser) GetKongaLocation(parameters map[string]interface{}, elements *goquery.Selection) {
	var result = make(map[string]string)
	elements.Eq(0).Each(func (i int, s *goquery.Selection){
		
		fullLoc := strings.ToLower(s.Text())

		// check state
		for _, state := range NigeriaStateList {
			if strings.Contains(fullLoc, state) {
				result["state"] = state
				fullLoc = strings.Replace(fullLoc, state, "", -1)
				break
			}
		}

		// ensure country is Nigeria
		if strings.Contains(fullLoc, "nigeria") {
			result["country"] = "nigeria"
			fullLoc = strings.Replace(fullLoc, "nigeria", "", -1)
		}

		// pack everything else in `others` key
		result["others"] = RemoveExcessWhiteSpace(RemovePunctuations(fullLoc))
	})
	analyser.Result = result
}

// takes the first selected element, gets konga description
// sets analyser.Result to a string (the description)
func (analyser *Analyser) GetKongaDescription(parameters map[string]interface{}, elements *goquery.Selection) {
	elements.Eq(0).Each(func (i int, s *goquery.Selection){
		desc := s.Text()
		analyser.Result = RemoveExcessWhiteSpace(desc)
	})
}