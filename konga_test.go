package pagequery

import (
	"testing"
	// "fmt"
	"github.com/stretchr/testify/assert"
)

func TestShouldGetLocation(t *testing.T) {

	testHTML := `
		<div class="sf_sellerinfo">
            <div class="vendorshopname">Konga</div>
            <div class="vendorlocation">
                <img src="http://www.konga.com/skin/frontend/imitate2_ee/konga/images/selffulfil/selffulfil_location_pin.png">
                    Lagos, Nigeria                
            </div>
            <div class="lc">
                <div class="sf_talktoseller">
                    <a class="no-phone talktosellercontainer" onclick="chatWithKonga()">
                        <span class="chat">Message Seller</span>
                    </a>
                </div>
            </div>
        </div>
	`
	testQuery := `
	{
		"getLocation": {
			"selector": "div.sf_sellerinfo div.vendorlocation",
			"funcs": ["GetKongaLocation"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(len(result["getLocation"].(map[string]string)), 0, "should contain atleast one item")
	assert.Equal(result["getLocation"].(map[string]string)["state"], "lagos", "should match state")
	assert.Equal(result["getLocation"].(map[string]string)["country"], "nigeria", "should match country")
}

func TestShouldGetDescription(t *testing.T) {

	testHTML := `
		<div class="long-description">
            <h2>Product details</h2>
            <div class="std" itemprop="description"><p style="text-align: justify;"><strong>Metrogypsie</strong> is a contemporary Nigerian brand that is all about acknowledging the style of ladies and gents on their grind. Metrogypsie is a luxurious brand that sets the tone for elegance and modern sophistication. It provides stylish ready to wear outfits for both men and women ranging from casual to formal wears.</p>
			<p style="text-align: justify;">Every wardrobe needs a stylish piece in it. Whether you're seeking out that special dress for an important occasion, a figure-flattering dress for work or fun, or looking for fashionable separates, it is essential to have a chic piece no matter what, to create a fabulous look. Try on this collarless stripped shirt for a fresh look.</p>
			<p style="text-align: justify;">Explore the various collections of Metrogypsie on <strong>Konga.com</strong> at the best prices with fast delivery service nationwide.</p></div>
        </div>
	`
	testQuery := `
	{
		"getDescription": {
			"selector": "div.long-description div[itemprop=\"description\"]",
			"funcs": ["GetKongaDescription"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(result["getDescription"], "", "should not be empty")


}

func TestShouldGetProductName(t *testing.T) {

	testHTML := `
		<div class="product-name">
		    <h1>
		        <span itemprop="brand" itemscope="" itemtype="http://schema.org/Brand"><span itemprop="name">Metrogypsie</span></span>
		        <span itemprop="name">Collarless Striped Shirt | Black &amp; White</span>
		        <span class="hidden"><a itemprop="url" href="http://www.konga.com/metrogypsie-collarless-striped-shirt-black-and-white-1266949">Product url</a></span>
		    </h1>
		</div>
	`
	testQuery := `
	{
		"getProductName": {
			"selector": "div.product-name",
			"funcs": ["GetText"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(len(result["getProductName"].([]string)), 0, "should contain atleast 1 value")
	assert.NotEqual(result["getProductName"].([]string)[0], "", "should not be empty")
}

func TestShouldGetAvailableSizes(t *testing.T) {

	testHTML_A := `
		<div>
		    <ul class="list-shoe_size">
				<li class="attribute-image"><button class="attribute-button-text 1278222_jpi_attr_1014 attribute-selected" id="1278222_jpi_option_1014-788" type="button" data-productid="1278222" data-attributeid="1014" data-optionid="788">1 <i class="icon"></i></button></li>
				<li class="attribute-image"><button class="attribute-button-text 1278222_jpi_attr_1014" id="1278222_jpi_option_1014-763" type="button" data-productid="1278222" data-attributeid="1014" data-optionid="763" disabled="disabled">2 <i class="icon"></i></button></li>
				<li class="attribute-image"><button class="attribute-button-text 1278222_jpi_attr_1014" id="1278222_jpi_option_1014-783" type="button" data-productid="1278222" data-attributeid="1014" data-optionid="783">3 <i class="icon"></i></button></li>
			</ul>
		</div>
	`

	testHTML_B := `
		<div>
			<ul class="list-dress_size">
				<li class="attribute-image"><button class="attribute-button-text 1302385_jpi_attr_1013 attribute-selected" id="1302385_jpi_option_1013-701" type="button" data-productid="1302385" data-attributeid="1013" data-optionid="701">1 <i class="icon"></i></button></li>
				<li class="attribute-image"><button class="attribute-button-text 1278222_jpi_attr_1014" id="1278222_jpi_option_1014-763" type="button" data-productid="1278222" data-attributeid="1014" data-optionid="763" disabled="disabled">2 <i class="icon"></i></button></li>
				<li class="attribute-image"><button class="attribute-button-text 1302385_jpi_attr_1013" id="1302385_jpi_option_1013-702" type="button" data-productid="1302385" data-attributeid="1013" data-optionid="702">3 <i class="icon"></i></button></li>
			</ul>
		</div>
	`

	testHTML_C := `
		<div>
			<ul class="list-shirt_size">
				<li class="attribute-image"><button class="attribute-button-text 1302385_jpi_attr_1013 attribute-selected" id="1302385_jpi_option_1013-701" type="button" data-productid="1302385" data-attributeid="1013" data-optionid="701">1 <i class="icon"></i></button></li>
				<li class="attribute-image"><button class="attribute-button-text 1278222_jpi_attr_1014" id="1278222_jpi_option_1014-763" type="button" data-productid="1278222" data-attributeid="1014" data-optionid="763" disabled="disabled">2 <i class="icon"></i></button></li>
				<li class="attribute-image"><button class="attribute-button-text 1302385_jpi_attr_1013" id="1302385_jpi_option_1013-702" type="button" data-productid="1302385" data-attributeid="1013" data-optionid="702">3 <i class="icon"></i></button></li>
			</ul>
		</div>
	`

	testQuery := `
	{
		"getAvailableSizes": {
			"selector": "div ul.list-shirt_size,div ul.list-shoe_size,div ul.list-dress_size",
            "funcs": ["GetEnabledChildrenText:{ \"childElemName\": \"li button\", \"disableAttr\":\"disabled\", \"disableAttrVal\":\"disabled\" }", "UseFirstIndexOfSlice"]
		}
	}`

	testQueryB := `
	{
		"getAvailableSizes": {
			"selector": "div ul.list-shirt_size,div ul.list-shoe_size,div ul.list-dress_size",
            "funcs": ["GetEnabledChildrenText:{ \"childElemName\": \"li button\", \"disableAttr\":\"disabled\", \"disableAttrVal\":\"disabled\" }", "UseFirstIndexOfSlice", "PackageAvailableSize:{ \"sizeUnit\":\"uk\" }"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML_A, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.Equal(len(result["getAvailableSizes"].([]string)), 2, "should contain number of expected items")
	assert.NotEqual(result["getAvailableSizes"].([]string)[1], "2", "should not match")

	pageQuery = PageQuery{}
	result, err = pageQuery.Read(testHTML_B, testQuery)
	assert.Nil(err)
	assert.Equal(len(result["getAvailableSizes"].([]string)), 2, "should contain number of expected items")
	assert.NotEqual(result["getAvailableSizes"].([]string)[1], "2", "should not match")

	pageQuery = PageQuery{}
	result, err = pageQuery.Read(testHTML_C, testQuery)
	assert.Nil(err)
	assert.Equal(len(result["getAvailableSizes"].([]string)), 2, "should contain number of expected items")
	assert.NotEqual(result["getAvailableSizes"].([]string)[1], "2", "should not match")

	pageQuery = PageQuery{}
	result, err = pageQuery.Read(testHTML_C, testQueryB)
	assert.Nil(err)
	assert.Equal(len(result["getAvailableSizes"].(map[string]interface{})), 2, "should contain number of expected items")
	assert.Equal(len(result["getAvailableSizes"].(map[string]interface{})["sizes"].([]string)), 2, "should contain 2 items")
	assert.Equal(result["getAvailableSizes"].(map[string]interface{})["unit"].(string), "uk", "should match")
}
