package pagequery

import (
	// "strings"
	"github.com/PuerkitoBio/goquery"
)

// creates location information 
// expects location data to be assigned to 
// Analyser.Result after being extracted with GetByRegex func
// Analyser.Result must be an array of string of captured group from GetByRegex func
// Analyser.Result must not be an error, else do nothing
func (analyser *Analyser) OLXLocation(parameters map[string]interface{}, elements *goquery.Selection) {
	result := make(map[string]string)
	switch val := analyser.Result.(type) {
	case []string:
		// expects 3 captured groups
		if len(val) == 3 {
			result["state"] = RemoveExcessWhiteSpace(val[2])
			result["city"] = RemoveExcessWhiteSpace(val[1])
			result["country"] = "nigeria"		
		}
	}
	analyser.Result = result
}
