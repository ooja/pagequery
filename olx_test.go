package pagequery

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestOLXLocation(t *testing.T) {

	testHTML := `
		<div class="product-location">Ejigbo, Lagos</div>
	`
	testQuery := `
	{
		"getLocation": {
			"selector": "div.product-location",
            "funcs": ["GetText", "UseFirstIndexOfSlice", "GetByRegex:{ \"rx\":\"(?i)^([a-z0-9- ]+),[\\\\s]*([a-z0-9- ]+)$\" }", "OLXLocation"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.Equal(len(result["getLocation"].(map[string]string)), 3, "should have 3 items")
	assert.Equal(result["getLocation"].(map[string]string)["state"], "Lagos", "should match")
	assert.Equal(result["getLocation"].(map[string]string)["city"], "Ejigbo", "should match")
	assert.Equal(result["getLocation"].(map[string]string)["country"], "nigeria", "should match")
}