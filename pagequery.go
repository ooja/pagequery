package pagequery

import (
	"github.com/PuerkitoBio/goquery"
	"bytes"
	"encoding/json"
	"errors"
    "strings"
    "regexp"
    // "fmt"
)

type PageQuery struct {
	analyser Analyser
    methodStartFunc func(methodParam map[string]interface{})
    methodEndFunc func(methodParam map[string]interface{}, result interface{})
}

// set method to call each time the analyse runs a method
func (pq *PageQuery) OnMethodStart(f func(methodParams map[string]interface{})){
    pq.methodStartFunc = f
}

// set method to call right before a method stops running
func (pq *PageQuery) OnMethodEnd(f func(methodParams map[string]interface{}, result interface{})){
    pq.methodEndFunc = f
}

// parse method name. extract name return a parsed parameter
func ParseMethodName(methodName string) (map[string]interface{}, error) {
    
    var result = make(map[string]interface{})

    if strings.Contains(methodName, ":") {

        methodSplit := strings.SplitN(methodName, ":", 2)
        var params map[string]interface{}
        
        if err := json.Unmarshal([]byte(methodSplit[1]), &params); err != nil {
            return result, errors.New("unable to parse method name: " + err.Error())
        } 

        result["name"] = methodSplit[0]
        result["parameters"] =  params
        return result, nil

    }  else {

        result["name"] = methodName
        return result, nil
    }
}

// process an html file using its query dsl
func(pq *PageQuery) Read(html, queryDSL string) (map[string]interface{}, error) {

	var result = make(map[string]interface{})

	// create goquery doc
	pageAsReader := bytes.NewReader([]byte(html))
    doc, err := goquery.
    NewDocumentFromReader(pageAsReader)
    if err != nil {
    	return result, errors.New("unable to parse html. " + err.Error())
    }

    // convert query dsl to json object
    var queryDSLData map[string]map[string]interface{} 
    if err := json.Unmarshal([]byte(queryDSL), &queryDSLData); err != nil {
        return result, errors.New("unable to parse query dsl json. " + err.Error())
    }

    // start analysis for each query key
    for key, queryData := range queryDSLData {
    	
    	selector := queryData["selector"]
    	funcs := queryData["funcs"]
    	selection := doc.Find(selector.(string))
    	analyser := Analyser{}
        
    	for _, methodName := range funcs.([]interface{}) {

            // parse method name
            methodParams, err := ParseMethodName(methodName.(string))
            if err != nil {
                return result, errors.New("unable to parse methodName ("+methodName.(string)+") with parameters: " + err.Error())
            }

            // call start event func
            if pq.methodStartFunc != nil {
                pq.methodStartFunc(methodParams)
            }
            
            // parameters
            parameters := make(map[string]interface{})
            if methodParams["parameters"] != nil {
                parameters = methodParams["parameters"].(map[string]interface{})
            }

            // run method
    		if err := analyser.Call(methodParams["name"].(string), parameters, selection); err != nil {
    			return result, err
    		}

            // call end event func
            if pq.methodEndFunc != nil {
                pq.methodEndFunc(methodParams, analyser.Result)
            }
    	}

    	result[key] = analyser.Result
    }

    return result, nil
}

// using an known list of terns, extracts the values of
// features/keys that match the any of the known terms.
// returns a new map of the matching known terms and the extracted values
func (pa *PageQuery) FindInFeatures(features map[string]interface{}, knownTermsData []map[string]string) map[string]interface{} {
    newResult := make(map[string]interface{})
    for _, termData := range knownTermsData {
        for key, value := range features {
            switch val := value.(type) {
            case string:
                // use rxTerm of term data to see if key matches
                r, err := regexp.Compile(termData["rxTerm"])

                // no regexp error
                if err == nil {
                    
                    // check key for a match against the known term regex
                    if r.MatchString(key) {

                        // find value using rxValue if present in terms data
                        if _, found := termData["rxValue"]; found {

                            r, err = regexp.Compile(termData["rxValue"])
                            if err == nil {
                                
                                result := make(map[string]string)
                                match := r.FindStringSubmatch(val)
                                
                                // if match length not equal to number of expected sub expression/group expected
                                // continue to next term
                                if len(match) != len(r.SubexpNames()) {
                                    continue
                                }

                                for i, name := range r.SubexpNames() {
                                    if name != "" {
                                        result[name] = match[i]
                                    }
                                }

                                // ensure the terms key and value key are present 
                                // to be sure that  there was a match
                                if termData["term"] != "" && result["value"] != "" {
                                    
                                    // return all captured group as result, else return only `value` group
                                    if termData["all"] != "" {
                                        newResult[termData["term"]] = result 

                                    } else {

                                        // return only the value group
                                        newResult[termData["term"]] = result["value"]
                                    }
                                }
                            }
                        } else {

                            // use existing value of key
                            if termData["term"] != "" {

                                // use explicit value if provided
                                if termData["value"] != "" { 
                                    val = termData["value"]
                                }

                                newResult[termData["term"]] =  val 
                            }
                        }
                    }
                }
            }
        }
    }
    return newResult
}
