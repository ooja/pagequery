package pagequery

import (
	"testing"
	"path/filepath"
	"os"
	// "fmt"
	"bitbucket.org/ooja/common"
	"github.com/stretchr/testify/assert"
)

func GetCurrentPath() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
    if err != nil {
        panic(dir)
    }
    return dir
}

func TestParseMethodNameWithoutParams(t *testing.T) {
	methodName := "MyMethod"
	methodParams, err := ParseMethodName(methodName)
	assert := assert.New(t)
	assert.Nil(err)
	assert.Equal(methodParams["name"], methodName, "should equal MyMethod")
	assert.Equal(len(methodParams), 1, "should contain only 'name' key")
	assert.Equal(methodParams["parameters"], nil, "should not contain parameters")
}

func TestParseMethodNameWithParams(t *testing.T) {
	methodName := `MyMethod:{ "name": "konga", "age": 23 }`
	methodParams, err := ParseMethodName(methodName)
	assert := assert.New(t)
	assert.Nil(err)
	assert.Equal(methodParams["name"], methodParams["name"], "should equal MyMethod")
	assert.Equal(len(methodParams), 2, "should contain 2 keys")
	assert.NotEqual(methodParams["parameters"], nil, "should contain parameters")
	assert.Equal(methodParams["parameters"].(map[string]interface{})["name"], "konga", "should contain param with name property")
}

func TestGetSRCOfAllImages(t *testing.T) {

	testHTML := `
		</head>
		<body>
			<div class="product"><img src="blah.png" /><img src="bleh.png" /></div>
		</body>
		</html>
	`

	testQuery := `
	{
		"productImages": {
			"selector": ".product",
			"funcs": ["GetSRCOfAllImages"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(result["productImages"], nil, "productImages key must be returned")
	assert.Equal(len(result["productImages"].([]string)), 2, "2 items must be returned")
	assert.Equal(result["productImages"].([]string)[0], "blah.png", "blah.png must be at index 0")
	assert.Equal(result["productImages"].([]string)[1], "bleh.png", "bleh.png must be at index 1")
}	

func TestGetSRC(t *testing.T) {

	testHTML := `
		</head>
		<body>
			<div class="product"><img src="blah.png" /><img src="bleh.png" /></div>
		</body>
		</html>
	`
	testQuery := `
	{
		"primaryImage": {
			"selector": ".product img:first-child",
			"funcs": ["GetSRC"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(result["primaryImage"], nil, "primaryImage key must be returned")
	assert.Equal(len(result["primaryImage"].([]string)), 1, "1 item must be returned")
	assert.Equal(result["primaryImage"].([]string)[0], "blah.png", "blah.png must be at index 0")
}	

func TestGetOneSRCString(t *testing.T) {

	testHTML := `
		</head>
		<body>
			<div class="product"><img src="blah.png" /><img src="bleh.png" /></div>
		</body>
		</html>
	`
	testQuery := `
	{
		"primaryImage": {
			"selector": ".product img:first-child",
			"funcs": ["GetOneSRC"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(result["primaryImage"], nil, "primaryImage key must be returned")
	assert.NotEqual(result["primaryImage"].(string), "", "must not be empty")
	assert.Equal(result["primaryImage"].(string), "blah.png", "blah.png expected")
}

func TestGetText(t *testing.T) {

	testHTML := `
		</head>
		<body>
			<div class="product"><img src="blah.png" /><img src="bleh.png" />ben <b>10</b></div>
		</body>
		</html>
	`
	testQuery := `
	{
		"getName": {
			"selector": ".product",
			"funcs": ["GetText"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(result["getName"], nil, "getName key must be returned")
	assert.Equal(len(result["getName"].([]string)), 1, "1 item must be returned")
	assert.Equal(result["getName"].([]string)[0], "ben 10", "text should match item at index 0")
}

func TestShouldGetStandardPrice(t *testing.T) {

	testHTML := `
		<body>
			<div class="price">price: $50,000.00 buy </div>
		</body>
	`
	testQuery := `
	{
		"getPrice": {
			"selector": ".price",
			"funcs": ["GetText", "GetStandardPrice"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(result["getPrice"], nil, "getPrice key must be returned")
	assert.Equal(len(result["getPrice"].(map[string]interface{})), 2, "must contain 2 keys")
	assert.Equal(result["getPrice"].(map[string]interface{})["currency"], "USD", "text should match currency")
	assert.Equal(result["getPrice"].(map[string]interface{})["amount"], 50000.0, "text should match amount")
}


func TestShouldTrimTerms(t *testing.T) {

	testHTML := `
		<body>
			<div class="title">The quick fox jumped, on the green slow fox owner</div>
		</body>
	`
	testQuery := `
	{
		"getTitle": {
			"selector": ".title",
			"funcs": ["GetText", "TrimTerms:{ \"terms\": [ \"jumped\", \"fox\", \",\" ]}", "Trim"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(result["getTitle"], nil, "getTitle key must be returned")
	assert.Equal(len(result["getTitle"].([]string)), 1, "1 item must be returned")
	assert.Equal(result["getTitle"].([]string)[0], "The quick on the green slow owner", "must match the expected text")
}

func TestShouldTrimTitleTerms(t *testing.T) {

	testHTML := `
		<body>
			<div class="title">Dell Inspiron 11-3148 Intel Core i3 - 4GB - 500GB  | Konga Nigeria</div>
		</body>
	`
	testQuery := `
	{
		"getTitle": {
			"selector": ".title",
			"funcs": ["GetText", "TrimTerms:{ \"terms\": [\"[|]+\",\"konga\",\"nigeria\"] }", "Trim"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.Equal(result["getTitle"].([]string)[0], "Dell Inspiron 11-3148 Intel Core i3 - 4GB - 500GB", "should match")
}

func TestShouldGetHREFFromLink(t *testing.T) {

	testHTML := `
		<body>
			<div class="title">
				<ul>
					<li><a href="google.com">Link 1</a></li>
					<li><a href="yahoo.com">Link 2</a></li>
				</ul>
			</div>
		</body>
	`
	testQuery := `
	{
		"getLinks": {
			"selector": "ul li a",
			"funcs": ["GetHREF"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.Equal(len(result["getLinks"].([]string)), 2, "should contain 2 links")
	assert.Equal(result["getLinks"].([]string)[0], "google.com", "should match google.com")
	assert.Equal(result["getLinks"].([]string)[1], "yahoo.com", "should match yahoo.com")
}

func TestShouldRemoveExcessWhiteSpace(t *testing.T) {
	result := RemoveExcessWhiteSpace(" the  man    is here ")
	assert := assert.New(t)
	assert.Equal(result, "the man is here", "must match")
}

func TestShouldGetArbitraryAttribute(t *testing.T) {

	testHTML := `
		<body>
			<div class="title" data-name="john"></div>
		</body>
	`
	testQuery := `
	{
		"getAttr": {
			"selector": "div.title",
			"funcs": ["GetAttr:{ \"attr\": \"data-name\" }"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.Equal(len(result["getAttr"].([]string)), 1, "should contain 1 item")
	assert.Equal(result["getAttr"].([]string)[0], "john", "should match")
}

func TestShouldGetKeyFeatures(t *testing.T) {

	testHTML := `
		<body>
			<div class="std key-features">
                <ul>
					<li style="text-align: justify;"><strong>Colour</strong>: Grey &amp; Black</li>
					<li style="text-align: justify;"><strong>Main Material</strong>: Polyester</li>
					<li style="text-align: justify;">High Quality</li>
				</ul>                    
			</div>
		</body>

	`
	testQuery := `
	{
		"getKeyFeatures": {
			"selector": ".key-features",
			"funcs": ["UL_GetKeyFeatures"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(len(result["getKeyFeatures"].(map[string]interface{})), 0, "should contain atleast 1 value")
	assert.Equal(result["getKeyFeatures"].(map[string]interface{})["colour"], "Grey & Black", "should match Grey & Black")
	assert.NotEqual(len(result["getKeyFeatures"].(map[string]interface{})["others"].([]string)), 0, "should contain atleast 1 item")
}

func TestShouldGetFullFeatures(t *testing.T) {

	testHTML := `
		<body>
			<div class="box-collateral box-additional">
        		<table class="data-table product_features_table" id="product-attribute-specs-table-2">
		            <colgroup><col width="25%">
		            <col>
		            </colgroup><tbody>
		                <tr>
		                    <th colspan="2" style="text-transform:capitalize !important;">Apparel                    </th>
		                </tr>
                        <tr>
		                    <td class="label">Brand</td>
		                    <td class="data">Metrogypsie</td>
		                </tr>
		                    <td class="label">Main Material</td>
		                    <td class="data">Polyester</td>
		                </tr>
                    </tbody>
		        </table>
    	        <table class="data-table product_features_table" id="product-attribute-specs-table-3">
		            <colgroup><col width="25%">
		            <col>
		            </colgroup><tbody>
		                <tr>
		                    <th colspan="2" style="text-transform:capitalize !important;">Shipping Location </th>
		                </tr>
		                                            <tr>
		                    <td class="label">Country</td>
		                    <td class="data">Nigeria</td>
		                </tr>
		                </tbody>
		        </table>
		</div>
		</body>
	`
	testQuery := `
	{
		"getFullFeatures": {
			"selector": ".box-collateral table",
			"funcs": ["Table_GetFullFeatures"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(len(result["getFullFeatures"].(map[string]interface{})), 0, "should contain atleast 1 value")
	assert.Equal(result["getFullFeatures"].(map[string]interface{})["brand"], "metrogypsie", "should match Metrogypsie")
}

func TestGetFromFeatures(t *testing.T) {

	testHTML := `
		<body>
			<div class="box-collateral box-additional">
        		<table class="data-table product_features_table" id="product-attribute-specs-table-2">
		            <colgroup><col width="25%">
		            <col>
		            </colgroup><tbody>
		                <tr>
		                    <th colspan="2" style="text-transform:capitalize !important;">Apparel                    </th>
		                </tr>
                        <tr>
		                    <td class="label">Brand</td>
		                    <td class="data">Metrogypsie</td>
		                </tr>
		                    <td class="label">Main Material</td>
		                    <td class="data">Polyester</td>
		                </tr>
                    </tbody>
		        </table>
    	        <table class="data-table product_features_table" id="product-attribute-specs-table-3">
		            <colgroup><col width="25%">
		            <col>
		            </colgroup><tbody>
		                <tr>
		                    <th colspan="2" style="text-transform:capitalize !important;">Shipping Location </th>
		                </tr>
		                                            <tr>
		                    <td class="label">Country</td>
		                    <td class="data">Nigeria</td>
		                </tr>
		                </tbody>
		        </table>
		</div>
		</body>
	`
	testQuery := `
	{
		"getFullFeatures": {
			"selector": ".box-collateral table",
			"funcs": ["Table_GetFullFeatures","GetFromFeatures:{ \"name\":\"brand\" }"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.Equal(result["getFullFeatures"].(string), "metrogypsie", "should contain atleast 1 value")

	testQuery = `
	{
		"getFullFeatures": {
			"selector": ".box-collateral table",
			"funcs": ["Table_GetFullFeatures","GetFromFeatures:{ \"name\":\"brand\" }", "ToStringSlice"]
		}
	}`

	pageQuery = PageQuery{}
	result, err = pageQuery.Read(testHTML, testQuery)
	assert.Nil(err)
	assert.NotEqual(len(result["getFullFeatures"].([]string)), 0, "should contain atleast 1 value")
	assert.Equal(result["getFullFeatures"].([]string)[0], "metrogypsie", "must match")
}


func TestShouldGetDefaultLocation(t *testing.T) {

	testHTML := `
	`
	testQuery := `
	{
		"getLocation": {
			"selector": "not_applicable",
            "funcs": ["SetDefaultLocation:{ \"country\":\"nigeria\" }"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(len(result["getLocation"].(map[string]string)), 0, "should contain atleast 1 value")
	assert.Equal(result["getLocation"].(map[string]string)["country"], "nigeria", "should match nigeria")
}

func TestShouldGetFirstIndex(t *testing.T) {

	testHTML := `
		<body>
			<div>
                <p>jenny</p>
                <p>benny</p>                   
			</div>
		</body>

	`
	testQuery := `
	{
		"getLocation": {
			"selector": "p",
            "funcs": ["GetText","UseFirstIndexOfSlice"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.Equal(result["getLocation"].(string), "jenny", "should match jenny")
}

func TestShouldGetULItemsText(t *testing.T) {

	testHTML := `
		<body>
			<div>
                <ul>
                	<li>1</li>
                	<li>2</li>
                	<li>3</li>
                </ul>  
                <ul>
                	<li>1</li>
                	<li class="myclass hide">2</li>
                	<li>3</li>
                </ul>                 
			</div>
		</body>

	`
	testQuery := `
	{
		"getULItem": {
			"selector": "ul",
            "funcs": ["UL_GetItemsText:{ \"disableAttr\": \"class\", \"disableAttrVal\": \"hide\" }"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(len(result["getULItem"].([][]string)), 0, "should not be empty")
	assert.Equal(len(result["getULItem"].([][]string)[0]), 3, "should equal 3")
	assert.Equal(len(result["getULItem"].([][]string)[1]), 2, "should equal 3")
	assert.NotEqual(result["getULItem"].([][]string)[1][1], "2", "should not equal 2")
}

func TestShouldGetULItemsTextUseUseFirstIndexOfSlice(t *testing.T) {

	testHTML := `
		<body>
			<div>
                <ul>
                	<li>1</li>
                	<li>2</li>
                	<li>3</li>
                </ul>  
                <ul>
                	<li>1</li>
                	<li class="myclass hide">2</li>
                	<li>3</li>
                </ul>                 
			</div>
		</body>

	`
	testQuery := `
	{
		"getULItem": {
			"selector": "ul",
            "funcs": ["UL_GetItemsText:{ \"disableAttr\": \"class\", \"disableAttrVal\": \"hide\" }", "UseFirstIndexOfSlice"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(len(result["getULItem"].([]string)), 0, "should not be empty")
	assert.Equal(len(result["getULItem"].([]string)), 3, "should equal 3")
}



func TestShouldGetEnabledChildrenText(t *testing.T) {

	testHTML := `
		<body>
			<div>
                <ul>
                	<li>1</li>
                	<li>2</li>
                	<li>3</li>
                </ul>  
                <ul>
                	<li><button>1</button></li>
                	<li><button>2</button></li>
                	<li><button>3</button></li>
                	<li><button disabled="disabled">4</button></li>
                </ul>   
                <span>1</span>
                <span class="hide">2</span>
                <span>3</span>              
			</div>
		</body>

	`
	testQueryA := `
	{
		"getEnabledChildrenText": {
			"selector": "ul:first-child",
            "funcs": ["GetEnabledChildrenText:{ \"childElemName\": \"li\" }", "UseFirstIndexOfSlice"]
		}
	}`

	testQueryB := `
	{
		"getEnabledChildrenText": {
			"selector": "ul:nth-child(2)",
            "funcs": ["GetEnabledChildrenText:{ \"childElemName\": \"button\", \"disableAttr\":\"disabled\", \"disableAttrVal\":\"disabled\" }", "UseFirstIndexOfSlice"]
		}
	}`

	testQueryC := `
	{
		"getEnabledChildrenText": {
			"selector": "div",
            "funcs": ["GetEnabledChildrenText:{ \"childElemName\": \"span\", \"disableAttr\":\"class\", \"disableAttrVal\":\"hide\" }", "UseFirstIndexOfSlice"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQueryA)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(len(result["getEnabledChildrenText"].([]string)), 0, "should not be empty")
	assert.Equal(len(result["getEnabledChildrenText"].([]string)), 3, "should equal 3")

	result, err = pageQuery.Read(testHTML, testQueryB)
	assert.Nil(err)
	assert.NotEqual(len(result["getEnabledChildrenText"].([]string)), 0, "should not be empty")
	assert.Equal(len(result["getEnabledChildrenText"].([]string)), 3, "should equal 3")

	result, err = pageQuery.Read(testHTML, testQueryB)
	assert.Nil(err)
	assert.NotEqual(len(result["getEnabledChildrenText"].([]string)), 0, "should not be empty")
	assert.Equal(len(result["getEnabledChildrenText"].([]string)), 3, "should equal 3")

	result, err = pageQuery.Read(testHTML, testQueryC)
	assert.Nil(err)
	assert.NotEqual(len(result["getEnabledChildrenText"].([]string)), 0, "should not be empty")
	assert.Equal(len(result["getEnabledChildrenText"].([]string)), 2, "should equal 3")
}

func TestShouldGetAllTD(t *testing.T) {

	testHTML := `
		<table>
		  <tr>
		    <th>Month</th>
		    <th>Savings</th>
		  </tr>
		  <tr>
		    <td>January</td>
		    <td>$100</td>
		  </tr>
		  <tr>
		    <td>February</td>
		    <td>$80</td>
		  </tr>
		</table>
	`
	testQuery := `
	{
		"getAllTD": {
			"selector": "table",
            "funcs": ["GetAllTD"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(len(result["getAllTD"].([]map[int][]string)), 0, "should not be empty")
	assert.Equal(len(result["getAllTD"].([]map[int][]string)[0]), 3, "map should contain 3 items")
	assert.Equal(len(result["getAllTD"].([]map[int][]string)[0][0]), 0, "row should contain 0 items")
	assert.Equal(len(result["getAllTD"].([]map[int][]string)[0][1]), 2, "row should contain 2 items")
	assert.Equal(len(result["getAllTD"].([]map[int][]string)[0][2]), 2, "row should contain 2 items")

	testQuery = `
	{
		"getAllTD": {
			"selector": "table",
            "funcs": ["GetAllTD:{ \"include_th\": true }"]
		}
	}`

	pageQuery = PageQuery{}
	result, err = pageQuery.Read(testHTML, testQuery)
	assert.Equal(len(result["getAllTD"].([]map[int][]string)[0][0]), 2, "row should contain 2 items")

	testQuery = `
	{
		"getAllTD": {
			"selector": "table",
            "funcs": ["GetAllTD", "UseFirstIndexOfSlice"]
		}
	}`

	pageQuery = PageQuery{}
	result, err = pageQuery.Read(testHTML, testQuery)
	assert.NotEqual(len(result["getAllTD"].(map[int][]string)), 0, "should not be empty")
}

func TestShouldGetByRegex(t *testing.T) {

	testHTML := `
		<span>Lagos-Ikeja</span>
	`
	testQuery := `
	{
		"getByRegex": {
			"selector": "span",
            "funcs": ["GetText", "UseFirstIndexOfSlice", "GetByRegex:{ \"rx\":\"(?i)^([a-z0-9- ]+)[\\\\s]*-[\\\\s]*([a-z0-9- ]+)$\" }"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.NotEqual(len(result["getByRegex"].([]string)), 0, "should not be empty")
	assert.Equal(result["getByRegex"].([]string)[1], "Lagos", "should match")
	assert.Equal(result["getByRegex"].([]string)[2], "Ikeja", "should match")
}


func TestShouldGetIndexOrDefault(t *testing.T) {

	testHTML := `
		<div class="zoomWindowContainer" style="width: 400px;">
			<div style="z-index: 999; overflow: hidden; text-align: center; width: 400px; height: 400px; float: left; border: 4px solid rgb(136, 136, 136); position: absolute; top: 0px; left: 461px; display: none; background-image: url(http://www.payporte.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/4/5/45_neon.png); background-color: rgb(255, 255, 255); background-size: 500px 469px; background-position: -14.412147505423px -77px; background-repeat: no-repeat;" class="zoomWindow">&nbsp;</div>
		</div>
	`
	testQuery := `
	{
		"getPrimaryImage": {
			"selector": "div.zoomWindowContainer div",
			"funcs": ["GetAttr:{ \"attr\": \"style\" }", "UseFirstIndexOfSlice", "GetByRegex:{ \"rx\": \"background-image: url\\\\((.*?)\\\\);\" }", "GetIndexOrDefault:{ \"index\": 1 }"]
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
	assert := assert.New(t)
	assert.Nil(err)
	assert.Nil(common.IGetError(result["getPrimaryImage"]), "should not reference error object")
	assert.NotEqual(result["getPrimaryImage"].(string), "", "must not be an empty string")
	assert.Equal(result["getPrimaryImage"].(string), "http://www.payporte.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/4/5/45_neon.png", "must not be an empty string")

}

func TestShouldGetDate(t *testing.T) {

	pageQuery := PageQuery{}
	assert := assert.New(t)

	olxDateHTML := `
		<div class="date">
			Today,08:54
		</div>
	`
	testQuery := `
	{
		"getDate": {
			"selector": "div.date",
			"funcs": ["GetText", "UseFirstIndexOfSlice", "GetDate"]
		}
	}`

	result, err := pageQuery.Read(olxDateHTML, testQuery)
	assert.Nil(err)
	assert.Nil(common.IGetError(result["getDate"]), "should not reference error object")
	isValidRFC3339, err := common.IsValidRFC3339Date(result["getDate"].(string))
	assert.Nil(err)
	assert.Equal(isValidRFC3339, true, "must be valid RFC3339 date")

	olxDateHTML2 := `
		<div class="date">
			08 Jun
		</div>
	`
	testQuery = `
	{
		"getDate": {
			"selector": "div.date",
			"funcs": ["GetText", "UseFirstIndexOfSlice", "GetDate"]
		}
	}`

	result, err = pageQuery.Read(olxDateHTML2, testQuery)
	assert.Nil(err)
	assert.Nil(common.IGetError(result["getDate"]), "should not reference error object")
	isValidRFC3339, err = common.IsValidRFC3339Date(result["getDate"].(string))
	assert.Nil(err)
	assert.Equal(isValidRFC3339, true, "must be valid RFC3339 date")

}