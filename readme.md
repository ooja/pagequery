# PageQuery
PageQuery extracts specific information from an html page by using css selector and special functions to get the data.

```golang
    testHTML := `
		</head>
		<body>
			<div class="product"><img src="blah.png" /><img src="bleh.png" /></div>
		</body>
		</html>
	`

	testQuery := `
	{
		"productImages": {
			"selector": ".product",
			"funcs": ["GetSRCOfAllImages"]    // functions to apply to the selected elements
		}
	}`

	pageQuery := PageQuery{}
	result, err := pageQuery.Read(testHTML, testQuery)
```

PageQuery works by selecting a tag using a css selector and applying one or more analyzer functions.

Add new function to analyser.go for more parsing functions. Better still, create a new file in the same pacakage as analyser.go.

# Expected Result For Fields
These are the current result fields and their expected type for our supported stores

```go

type Result struct {
    title                               []string
    brand                               []string
    primaryImage                        string
    otherImage                          []string
    keyFeatures                         map[string]interface{}
    fullFeatures                        map[string]interface{}
    location                            map[string]string
    avaliableShirtSizes                 []string
    productName                         []string
    prices                              map[string]map[string]string
    longDescription                     string                  // optional if shortDescription is provided
    shortDescription                    string                  // optional if longDescription is provided
    postedDate                          string                  // date doc was posted 
}
```







